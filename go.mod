module github.com/jamesog/scan

require (
	cloud.google.com/go v0.9.0 // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-chi/render v1.0.0
	github.com/gorilla/context v0.0.0-20160226214623-1ea25387ff6f // indirect
	github.com/gorilla/securecookie v0.0.0-20160422134519-667fe4e3466a
	github.com/gorilla/sessions v0.0.0-20160922145804-ca9ada445741
	github.com/mattn/go-sqlite3 v1.6.0
	github.com/pressly/goose v2.2.0+incompatible
	github.com/prometheus/client_golang v0.9.1
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90 // indirect
	github.com/prometheus/common v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67
	golang.org/x/oauth2 v0.0.0-20170517174439-f047394b6d14
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/appengine v1.0.0 // indirect
)
